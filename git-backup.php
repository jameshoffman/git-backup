<?php
	// Save a BitBucket user's repositories
	
	//
	// Credentials
	// 		Generate appkey from bitbucket.org -> Bitbucket Settings -> App passwords
	//		Give permission: Repository > Read 
	//
	
	$username = "";
	$appkey = "";
	
	//
	// Fetch repositories list
	//

	$repositoriesRequest = curl_init(); 
	
	curl_setopt($repositoriesRequest, CURLOPT_USERPWD, "{$username}:{$appkey}");
	curl_setopt($repositoriesRequest, CURLOPT_RETURNTRANSFER, true);
	
	$repositories = [];
 	
	$nextPageUrl = "https://api.bitbucket.org/2.0/repositories/{$username}?role=member";
	while($nextPageUrl) {
		curl_setopt($repositoriesRequest, CURLOPT_URL, $nextPageUrl); 
		$repositoriesRequestResult = curl_exec($repositoriesRequest);
		
		$repositoriesJson = json_decode($repositoriesRequestResult, true);
		
		foreach ($repositoriesJson["values"] as $repositoryJson) {
			$repositoryName = $repositoryJson["full_name"];
			$repositories[] = $repositoryName;
			
			echo("{$repositoryName}\n");
		}
		
		$nextPageUrl = $repositoriesJson["next"];
	}
	
	curl_close($repositoriesRequest);
	
	//
	// Download repositories master branch as zip
	//

	$downloadRequest = curl_init(); 
	curl_setopt($downloadRequest, CURLOPT_USERPWD, "{$username}:{$appkey}");
	
	$outputDirectoryPath = __DIR__ . DIRECTORY_SEPARATOR . "git-backup-repositories";
	mkdir($outputDirectoryPath);
	
	foreach ($repositories as $repository) {	
		$outputFilePath = $outputDirectoryPath . DIRECTORY_SEPARATOR . str_replace("/", "-", $repository) . ".zip";
		
		echo("Downloading {$outputFilePath}\n");
		$outputFile = fopen($outputFilePath, "w");
		
		curl_setopt($downloadRequest, CURLOPT_FILE, $outputFile);
		curl_setopt($downloadRequest, CURLOPT_URL, "https://bitbucket.org/{$repository}/get/master.zip");
		
		curl_exec($downloadRequest);
		
		fclose($outputFile);
		echo("-> Saved\n\n");
	}
	
	curl_close($downloadRequest);
	
	$statusFile = fopen($outputDirectoryPath . DIRECTORY_SEPARATOR . "_last-backup.txt", "w");
	fwrite($statusFile, date("Y-m-d"));
	fclose($statusFile);
?>