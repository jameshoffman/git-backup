<h1>Git Backup Status</h1>


<?php
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);

	$gitBackupPath = ""; // ex: /home/bk/

	//
	// Validate last backup date
	//

	$latestBackupDate = file_get_contents("{$gitBackupPath}git-backup-repositories/_last-backup.txt");
	$latestBackupDateColor = "darkgreen";

	if($latestBackupDate === false) {
		$latestBackupDate = "N/A";
		$latestBackupDateColor = "red";
	} else {
		$dateValidation = new DateTime("3 days ago");
        	$latestBackup = new DateTime($latestBackupDate);

        	if($latestBackup < $dateValidation) {
                	$latestBackupDateColor = "red"; 
        	}
	}

	echo("<p>Latest backup: <span style='color:{$latestBackupDateColor}'>{$latestBackupDate}</span></p>");

	//
	// Show backup size and count
	//

	$backupSize = getDirectorySize("{$gitBackupPath}git-backup-repositories/");
	if($backupSize < 1024) {
                $backupSize = $backupSize . " Bytes";
        }
        else if( ($backupSize < 1048576) && ($backupSize > 1023) ) {
                $backupSize = round($backupSize/1024, 1) . " KB";
        }
        else if( ($backupSize < 1073741824) && ($backupSize > 1048575)) {
                $backupSize = round($backupSize/1048576, 1) . " MB";
        }
        else {
        	$backupSize = round($backupSize/1073741824, 1) . " GB";
	}

	echo("<p>Size: {$backupSize}</p>");


	function getDirectorySize($path) {
	// https://gist.github.com/eusonlito/5099936

		$size = 0;
    	
		foreach (glob(rtrim($path, '/').'/*', GLOB_NOSORT) as $each) {
        		$size += is_file($each) ? filesize($each) : getDirectorySize($each);
    		}

		return $size;
	}

	//
	// Show files
	//

	$backupFiles = scandir("{$gitBackupPath}git-backup-repositories/");

	echo("<p>" . (count($backupFiles) - 3) . " files:</p>"); // -3 to remove ., .., and _last-backup.txt 
	
	foreach($backupFiles as $file) {
		if (in_array($file, [".", "..", "_last-backup.txt"]) === false) {
			echo("<p style='margin-left: 16px;'>{$file}</p>");
		}
	}
?>